/**
 *@NApiVersion 2.1
 *@NScriptType UserEventScript
 * @author: Thiago Ramos
 */
//define(["../models/Laywer","../models/Priest"], function(layer,priest) {
//    define([], function() {
define([
    "../models/Lawyer",
    "../models/Priest",
    "../constants/Constants",
], function (Lawyer, Priest, Constants) {
    const entityFieldName = Constants.customentity.conjuge;

    function isPerson(context) {
        return context.newRecord.getValue({ fieldId: "isperson" }) == "T";
    }

    /**
     * Handle before submit event on create  record
     * @param {*} context
     * @returns void
     */
    function handleConjugeOnCreate(context) {
        log.debug("handleConjugeOnCreate", "handleConjugeOnCreate");
        log.debug("handleConjugeOnCreate", "isPerson? " + isPerson(context));

        if (!isPerson(context)) {
            return;
        }
        let newConjuge = context.newRecord.getValue({ fieldId: entityFieldName });
        log.debug("handleConjugeOnCreate", "new conjuge: "+newConjuge);
        
        if (newConjuge) {
            let priest = Priest.build();
            priest.setRecordType(context.newRecord.type);
            priest.setHusbandId(context.newRecord.id);
            priest.setWifeId(newConjuge);
            priest.marry();

        }
    }

    /**
     * Handle
     * @param {*} context
     * @returns
     */

    function handleConjugeOnEdit(context) {
        log.debug("handleConjugeOnEdit", "context: " + context);
        log.debug("handleConjugeOnEdit", "isperson? " + isPerson(context));
        if (!isPerson(context)) {
            return;
        }

        let oldConjuge = context.oldRecord.getValue({ fieldId: entityFieldName });
        let newConjuge = context.newRecord.getValue({ fieldId: entityFieldName });

        log.debug("handleConjugeOnEdit", "oldConjuge: " + oldConjuge);
        log.debug("handleConjugeOnEdit", "newConjuge: " + newConjuge);

        if (conjugeValueHasChanged(oldConjuge, newConjuge)) {
            //Se o valor do conjuge mudou, entao faz o casamento
            if (newConjuge) {
                //make the marriage
                let priest = Priest.build();
                priest.setRecordType(context.newRecord.type);
                priest.setHusbandId(context.newRecord.id);
                priest.setWifeId(newConjuge);
                priest.marry();

                //Se estava casado e mudou de conjuge,
                if (oldConjuge) {
                    //desmarca o casamento do antigo conjuge
                    let lawyer = Lawyer.build();
                    lawyer.setRecordType(context.oldRecord.type);
                    lawyer.setRecordId(oldConjuge);
                    lawyer.divorce();
                }
            }
        } else {
            log.debug("handleConjugeOnEdit", "Conjuge value has not changed");
        }
    }
    /**
     * return true if the value of the conjuge has changed
     * @param {*} oldConjuge
     * @param {*} newConjuge
     * @returns
     */
    function conjugeValueHasChanged(oldConjuge, newConjuge) {
        return oldConjuge !== newConjuge;
    }
    /**
     * Run on after submit Entry point
     * @param {*} context
     * @returns
     */
    function afterSubmit(context) {
        //run only on create and edit
        const eventRouter = {
            //set here the function that handles the event.
            create: handleConjugeOnCreate,
            edit: handleConjugeOnEdit,
        };
        //check if the event is defined as a function object in the eventRouter
        if (typeof eventRouter[context.type] !== "function") {
            return;
        }

        //run the event router
        eventRouter[context.type](context);
    }

    return {
        //    beforeLoad: beforeLoad,
        //    beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit,
    };
});

/**
 *@NApiVersion 2.1
 *@NScriptType UserEventScript
 @author Thiago Ramos em  18/11/2021    
 */
define(["N/record"], (record) => {
    
    
    const entityFieldName = {conjuge:"custentity_rsc_conjuge"};
        
    /**
     * 
     * @param {*} conjugeSourceId 
     * @param {*} conjugeTargetId 
     * @param {*} removeLink 
     * //atualiza o campo conjuge do cadastro do conjuge.
     *   //Se Abrir o cadastro do joao e selecionar maria como conjuge, após salvar, maria deve ter joao como conjuge
     *   //Joao source, Maria target
     *
     */
    function updateConjuge(conjugeSourceId, conjugeTargetId, removeLink){
        //atualiza o campo conjuge no cadastro target.
        log.audit("updateConjuge", "conjugeSourceId: " + conjugeSourceId + " conjugeTargetId: " + conjugeTargetId+" removeLink? "+removeLink);

        try{    
            if(conjugeTargetId){
                // carrega o registro do conjuge target
                let conjugeRecord = record.load({
                        type: record.Type.CUSTOMER,
                    id: conjugeTargetId, //maria
                    isDynamic: false
                }); 
                log.audit("updateConjuge", "conjugeRecord : " + conjugeRecord.getValue("entityid"));
                //remove o link conjuge.
                if(removeLink){
                    log.audit("updateConjuge", "conjugeSourceId now is '' ");
                    conjugeSourceId = "";
                }
                conjugeRecord.setValue({
                    fieldId: EntityFieldName.conjuge,
                    value: conjugeSourceId  //Joao
                });
               
                let id = conjugeRecord.save();
                log.audit("updatedConjuge", "id: " + id);

            }
        }catch(e){
            log.error({
                title: 'Error',
                details: e
            });
        }
    }

    /**
     * 
     * @param {*} context 
     * @returns void
     */

    function isPerson(context){
        return ( context.newRecord.getValue({fieldId:"isperson"}) == "T");
    }

    /*   
    function checkAndUpdateConjugeFieldValue(context){
                
        log.audit("checkAndUpdateConjugeFieldValue", "running checkAndUpdateConjugeFieldValue on "+ context.type +" context");
        
        //verifica se pessoa fisica
        //atualiza somente se for pessoa fisica 
        if (!isPerson(context)){
            return;
        }

        
        let conjugeField = {fieldId:EntityFieldName.conjuge}
        
        //Pega valor do campo custentity_rsc_conjuge
        let newConjuge = context.newRecord.getValue(conjugeField);
        let oldConjuge =  context.newRecord.getValue(conjugeField);
        
        //se for contexto type = edit entao pega valor do oldRecord
        //se for contexto type = create entao pega valor do newRecord, pois o oldRecord nao existe
        if(context.type == "edit"){
            oldConjuge = context.oldRecord.getValue(conjugeField);
        }


        let removeLink = false;
        //validar quando está criando um novo registro
        log.audit("checkAndUpdateConjugeFieldValue", "newConjuge: " + newConjuge + " oldConjuge: " + oldConjuge);
        //verifica se o valor do campo conjuge foi alterado
        if(conjugeValueHasChanged(oldConjuge, newConjuge)){
            //verifica se o valor do campo conjuge foi alterado para vazio 
            if(newConjuge === ""){
                newConjuge = oldConjuge;
                //remove  do link do conjuge também
                removeLink = true;    
                log.audit("checkAndUpdateConjugeFieldValue", "removing link");
                
            }
            updateConjuge(context.newRecord.getValue({fieldId:"id"}), newConjuge, removeLink);
        }


    }
    */


    /**
     * 
     * @param {*} oldConjuge  id do conjuge antigo
     * @param {*} newConjuge    id do conjuge novo
     * @returns 
     * //Verifica se o campo conjuge teve o valor modificado
     */
    function conjugeValueHasChanged(oldConjuge, newConjuge){
        return oldConjuge !== newConjuge;
    }


    function handleConjugeOnCreate(context){
        
        log.audit("handleConjugeUpdateOnCreate", "----- handleConjugeUpdateOnCreate -----");
        if (!isPerson(context)){
            return;
        }
        
        let conjugeField = {fieldId:EntityFieldName.conjuge}
        
        //Pega valor do campo custentity_rsc_conjuge
        let newConjuge =  context.newRecord.getValue(conjugeField);
        log.audit("handleConjugeUpdateOnCreate", "Conjuge: " + newConjuge);
        //verifica se o valor do campo conjuge foi alterado
        updateConjuge(context.newRecord.getValue({fieldId:"id"}), newConjuge, false);
    }
    

    function handleConjugeOnEdit(context){
        log.audit("handleConjugeUpdateOnEdit", "----- handleConjugeUpdateOnEdit -----");
        if (!isPerson(context)){
            return;
        }
        
        let conjugeField = {fieldId:EntityFieldName.conjuge}
        //Pega valor do campo custentity_rsc_conjuge
        let newConjuge =  context.newRecord.getValue(conjugeField);
        let oldConjuge =  context.oldRecord.getValue(conjugeField);
        let removeLink = false;
        
        //altera somente se o valor do campo conjuge foi alterado
        if(conjugeValueHasChanged(oldConjuge, newConjuge)){
            //verifica se o valor do campo conjuge foi alterado para vazio 
            if(newConjuge === ""){
                newConjuge = oldConjuge;
                //remove  do link do conjuge também
                removeLink = true;    
                log.audit("handleConjugeOnEdit", "removing link");
                
            }
            updateConjuge(context.newRecord.getValue({fieldId:"id"}), newConjuge, removeLink);
        }

        
    }

    
    /**
     * 
     * @param {*} context 
     * @returns 
     * //Run on the UserEvent script's entry point  (after submit)
     */
    function afterSubmit(context) {
        
        //run only on create and edit
        const eventRouter = {
            //set here the function that handles the event.
            'create': handleConjugeOnCreate,
            'edit'  : handleConjugeOnEdit
            
        };
        //check if the event is defined as a function object in the eventRouter       
        if ( typeof eventRouter[context.type]  !== 'function'){
            return;
        }
        
        //run the event router
        eventRouter[context.type](context);
    
    }

    return {
        //beforeLoad: beforeLoad,
        //beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    }
});

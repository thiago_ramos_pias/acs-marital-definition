/**
 *@NApiVersion 2.1
 * @author: Thiago Ramos 
*/
define(["N/record","../constants/Constants"], (nsRecord,Constants) => {
    const entityFieldName = Constants.customentity.conjuge;

    class Lawyer {
        constructor() {
            this.recordType,
                this.recordId
            this.print();
        }
        setRecordType(recordType) {
            this.recordType = recordType;
        }
        setRecordId(recordId) {
            this.recordId = recordId;
        }
        getRecordType() {
            return this.recordType;
        }
        getRecordId() {
            return this.recordId;
        }
        isRecordTypeNull() {
            return (this.recordType == null || this.recordType == '' || this.recordType == undefined);
        }
        isRecordIdNull() {
            return (this.recordId == null || this.recordId == '' || this.recordId == 'undefined');
        }
        /**
         * 
         * @returns N/record
         */
        divorce() {
            log.debug("Lawyer.divorce", "Lawyer.divorce");
            if (this.isRecordIdNull() || this.isRecordTypeNull()) {
                throw new Error('Record type or record id is invalid');
            }
            const record = nsRecord.load({
                type: this.recordType,
                id: this.recordId,
                isDynamic: false
            
            });
            record.setValue({ fieldId: entityFieldName, value: '' });
            return record.save();
        }
        print() {
            log.debug("print", "Instanciating Lawyer class");
        }


    }

    return {
        build: () => {
            return new Lawyer();
        }
    }



});
/**
 *@NApiVersion 2.1
 * @author: Thiago Ramos 
*/
define(["N/record","../constants/Constants"], (nsRecord,Constants) => {
    const entityFieldName = Constants.customentity.conjuge;

    class Priest{
        constructor(){
            this.recordType,
            this.husbandId,
            this.wifeId
        }
        getRecordType()  {
            return this.recordType;
        }
        setRecordType(recordType){
            this.recordType = recordType;
        }    
        
        getHusbandId() {
            return this.husbandId;
        }
        setHusbandId(husbandId){
            this.husbandId = husbandId;
        }
        getWifeId(){
            return this.wifeId;
        }
        setWifeId(wifeId){
            this.wifeId = wifeId;
        }
        _isValidWedding(){
            log.debug("Priest._isvalidWedding", "husbandId: " + this.husbandId + " wifeId: " + this.wifeId);
            if(this.husbandId && this.wifeId){
                return true;
            }
            return false;
        }

        //In this Use case, where the user is currently editing or creating a record,
        //you have to assume that the current record been created or edited is the husband and don't need to
        // have the field value set to the wife.
        //the only record that needs to be set to, is the wife's record.
        //Even if the user is editing the record that represents the wife, the script will take it as the husband and only will
        //update the wife's record, wich is real life is the husband's record.
        marry(){

            if (!this._isValidWedding()){
                throw new Error("Id of husband and wife must be set");
            }
            let wife = nsRecord.load({
                type: this.getRecordType(),
                id: this.getWifeId(), //maria
                isDynamic: false
            });

            
        
            wife.setValue({
                fieldId: entityFieldName,
                value: this.getHusbandId()
            });

       
            let id = wife.save();
            log.audit("Priest.marry", "declare now that "+this.getRecordType()+" "+this.getHusbandId() +" is now associated with "+this.getRecordType()+" "+this.getWifeId());
          
        }
    }
    
    return {
        build : () => {
            return new Priest();
        } 
    }

    
});